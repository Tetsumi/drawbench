# -*- coding: utf-8 -*-
"""
Drawing Benchmark

Authors:
    Tetsumi <tetsumi@vmail.me>
"""

#from bench_sfml import BenchSfml
from bench_pygame import BenchPygame
from bench_pyglet import BenchPyglet
from bench_template import BenchTemplate

if __name__ == "__main__":
    #BenchSfml().time_versus(BenchPygame(), BenchPyglet())
    BenchSfml().time_all()
    BenchPygame().time_all()
    BenchPyglet().time_all()
    
    
