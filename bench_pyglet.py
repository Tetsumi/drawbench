# -*- coding: utf-8 -*-
"""
Drawing Benchmark

Authors:
    Tetsumi <tetsumi@vmail.me>
"""

import pyglet
from bench import Bench

class BenchPyglet (Bench):
    def __init__ (self):
        super().__init__()
        self.window = pyglet.window.Window(width  = Bench.SCREEN_SIZE[0],
                                           height = Bench.SCREEN_SIZE[1],
                                           vsync  = False)
    def flip (self):
        self.window.flip()
    def clear (self):
        self.window.clear()
    def color (self, value):
        return (value >> 24 & 0xFF,
                value >> 16 & 0xFF,
                value >>  8 & 0xFF,
                value & 0xFF)
    def fill (self, color):
        pyglet.gl.glClearColor(*color)
        self.window.clear()
    def rectangle (self, left, top, width, height, color):
        pyglet.gl.glColor4i(*color)
        pyglet.graphics.draw(4,
                             pyglet.gl.GL_QUADS,
                             ('v2i', (left, top,
                                      left + width, top,
                                      left + width, top + height,
                                      left, top + height)))
    def line (self, start, end, color):
        pyglet.gl.glColor4i(*color)
        pyglet.graphics.draw(2,
                             pyglet.gl.GL_LINES,
                             ('v2i', (start[0], start[1], end[0], end[1])))
    def point (self, pos, color):
        pyglet.gl.glColor4i(*color)
        pyglet.graphics.draw(1,
                             pyglet.gl.GL_POINTS,
                             ('v2i', pos))
    def texture (self, pos, texture):
        texture.blit(pos[0], pos[1])
    def load_texture (self, filename):
        return pyglet.image.load(filename).get_texture()
