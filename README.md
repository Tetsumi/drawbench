A simple benchmark for drawing functions.

benchmarks have been implemented for

* PyGame
* PySFML
* Pyglet

Some results
-------------
cpu: intel e8400  
gpu: amd radeonhd 5770  
os: windows 7 x64  
python: cpython 3.4.1 [MSC v.1600 64 bit (AMD64)]  
pygame: 1.9.2a0  
pysfml: 2.1  


                     | BenchSfml           | BenchPygame
    ---------------- | ------------------- | -------------
    clear            | 0.04730633667591212 |  5.118449157243442
    flip             | 1.960385065073602   | 15.003800469383236
    random_fill      | 0.13519539150841853 |  5.419844847450605
    random_rectangle | 0.3114131854064164  |  0.5797827181563129
    random_line      | 0.24026353744399698 |  0.2309154683162511
    random_point     | 0.1756340516321524  |  0.13631235331758873
    random_texture   | 0.18263759334329777 |  4.935151354810877 
    random_color     | 0.05742521869003614 |  0.04774943460635939


cpu: intel e8400  
gpu: amd radeonhd 5770  
os: inux 3.16.2-1 x86_64  
driver: amd catalyst 14.20  
python: Python 3.4.1 [GCC 4.9.0 20140507 (prerelease)]  
pygame: 1.9.2a0  
pysfml: 2.1  


                     | BenchSfml    | BenchPygame 
    ---------------- | ------------ | ------------
    clear            |   0.02899228 |   3.71503097
    flip             |   0.57127908 |  11.10972306
    random_fill      |   0.11282259 |   3.71757931
    random_rectangle |   0.43120027 |   0.67195997
    random_line      |   0.34217624 |   0.22804516
    random_point     |   0.27363743 |   0.13839571
    random_texture   |   0.20257424 |   6.29448894
    random_color     |   0.05593539 |   0.04698132

