# -*- coding: utf-8 -*-
"""
Drawing Benchmark

Authors:
    Tetsumi <tetsumi@vmail.me>
"""

import pygame
import atexit
from bench import Bench

pygame.init()
atexit.register(pygame.quit)

class BenchPygame (Bench):
    def __init__ (self):
        super().__init__()
        self.display = pygame.display.set_mode(Bench.SCREEN_SIZE, 
                                               pygame.DOUBLEBUF | pygame.HWSURFACE, 
                                               Bench.SCREEN_DEPTH)
        self.surface = pygame.display.get_surface()
    def flip (self):
        pygame.display.flip()
    def clear (self):
        self.surface.fill(0)
    def color (self, value):
        return pygame.Color(value)
    def fill (self, color):
        self.surface.fill(color)
    def rectangle (self, left, top, width, height, color):
        rect = pygame.Rect(left, top, width, height)
        pygame.draw.rect(self.surface, color, rect) 
    def line (self, start, end, color):
        pygame.draw.line(self.surface, color, start, end) 
    def point (self, pos, color):
        self.surface.set_at(pos, color)
    def texture (self, pos, texture):
        self.surface.blit(texture, pos)
    def load_texture (self, filename):
        return pygame.image.load(filename)
