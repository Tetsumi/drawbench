# -*- coding: utf-8 -*-
"""
Drawing Benchmark

Authors:
    Tetsumi <tetsumi@vmail.me>
"""
import sfml.sf as sfml

from bench import Bench

class BenchSfml (Bench):
    def __init__ (self):
        super().__init__()
        mode = sfml.VideoMode(Bench.SCREEN_SIZE[0], Bench.SCREEN_SIZE[1])
        self.surface = sfml.RenderWindow(mode, "Bench")
    def flip (self):
        self.surface.display()
    def clear (self):
        self.surface.clear()
    def fill (self, color):
        self.surface.clear(color)               
    def rectangle (self, left, top, width, height, color):
        rectangle = sfml.RectangleShape()
        rectangle.position = (left, top)
        rectangle.size = (width, height)
        rectangle.fill_color = color
        self.surface.draw(rectangle)
    def line (self, start, end, color):
        line = sfml.VertexArray(sfml.PrimitiveType.LINES, 2)
        line[0].position = start
        line[0].color = color
        line[1].position = end
        line[1].color = color                       
        self.surface.draw(line)           
    def point (self, pos, color):
        point = sfml.VertexArray(sfml.PrimitiveType.POINTS, 1)
        point[0].position = pos
        point[0].color = color
        self.surface.draw(point)
    def texture (self, pos, texture):
        texture.position = pos
        self.surface.draw(texture)
    def color (self, value):
        return sfml.Color(value >> 24 & 0xFF,
                          value >> 16 & 0xFF,
                          value >>  8 & 0xFF,
                          value & 0xFF)
    def load_texture (self, filename):
        return sfml.Sprite(sfml.Texture.from_file(filename))
