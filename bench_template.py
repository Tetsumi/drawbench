# -*- coding: utf-8 -*-
"""
Drawing Benchmark

Authors:
    Tetsumi <tetsumi@vmail.me>
"""


from bench import Bench

class BenchTemplate (Bench):
    def __init__ (self):
        super().__init__()
    def flip (self):
        """Update the screen."""
        pass
    def clear (self):
        """Clear the screen."""
        pass
    def color (self, value):
        """Return a color object from a 32 bits unsigned."""
        pass
    def fill (self, color):
        """Fill the screen with color."""
        pass
    def rectangle (self, left, top, width, height, color):
        """Draw a rectangle with given attributes."""
        pass
    def line (self, start, end, color):
        """Draw a line with given attributes."""
        pass
    def point (self, pos, color):
        """Draw a point with given attributes."""
        pass
    def texture (self, pos, texture):
        """Draw the given texture at the given position."""
        pass
    def load_texture (self, filename):
        """Return a texture object."""
        pass
