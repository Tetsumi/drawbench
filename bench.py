# -*- coding: utf-8 -*-
"""
Drawing Benchmark

Authors:
    Tetsumi <tetsumi@vmail.me>
"""

import random
import time
import os

__VERSION__ = "1.2"

def format_float (f):
    return "{0:12.8f}".format(f)
            
# Format of table is column major
#  [(header1|row1|row2),
#   (header2|row1|row2),
#   ...]   
def print_table (table):
    column_amount = len(table)
    columns_width = [0] * column_amount
    row_amount = len(table[0])    
    # grab columns width and print the headers
    for i in range(column_amount):
        columns_width[i] = max([len(x) for x in table[i]])
        print("{0:<{1}}".format(table[i][0],columns_width[i]),end="")
        if i+1<column_amount:
            print(" | ", end="")
    print("") 
    # print separation row
    for i in range(column_amount):
        print("-" * columns_width[i], end="")
        if i+1<column_amount:
            print(" | ", end="")
    print("")   
    # print elements rows
    for i in range(1, row_amount):
        for j in range(column_amount):
            print("{0:<{1}}".format(table[j][i],columns_width[j]),end="")
            if j + 1 < column_amount:
                print(" | ", end="")
        print("") 
        
class Bench:
    SCREEN_SIZE = (1024, 768)
    SCREEN_DEPTH = 32
    SEED = 2947359069
    REPEAT = 10000
    TEXTURES = [os.path.join(os.path.curdir,  "res", "64_64.bmp"),
                os.path.join(os.path.curdir,  "res", "256_256.bmp"),
                os.path.join(os.path.curdir,  "res", "512_256.bmp"),
                os.path.join(os.path.curdir,  "res", "512_512.bmp")]
    def __init__ (self):
        self.random = random.Random()
        self.textures = []
        for texture in Bench.TEXTURES:
            self.textures.append(self.load_texture(texture))
        self.to_bench = [self.clear,
                         self.flip,
                         self.random_fill,
                         self.random_rectangle,
                         self.random_line,
                         self.random_point,
                         self.random_texture,
                         self.random_color]
    def time (self, method):
        self.random.seed(Bench.SEED)
        delta = 0.0
        for x in range(Bench.REPEAT):
            p_time = time.perf_counter()
            method()  
            delta += time.perf_counter() - p_time
            #self.flip()
        return delta
    def time_print (self, method):
        method_name = method.__func__.__name__
        delta = self.time(method)
        print(method_name, ":", delta)
    def time_versus (self, *challengers):
        columns = [[""], 
                   [self.__class__.__name__]]
        for challenger in challengers:
            columns.append([challenger.__class__.__name__])
        for i in range(len(self.to_bench)):
            columns[0].append(self.to_bench[i].__func__.__name__)
            columns[1].append(format_float(self.time(self.to_bench[i])))
            for i, challenger in enumerate(challengers):
                time = format_float(challenger.time(challenger.to_bench[i]))
                columns[2 + i].append(time)
        print_table(columns)
    def time_all (self):
        columns = [[""],
                   [self.__class__.__name__]]
        for method in self.to_bench:
            columns[0].append(method.__func__.__name__)
            columns[1].append(format_float(self.time(method)))
        print_table(columns)
    def random_fill (self):
        self.fill(self.random_color())
    def random_rectangle (self):
        left = self.random.randint(0, Bench.SCREEN_SIZE[0])
        top = self.random.randint(0, Bench.SCREEN_SIZE[1])
        width = self.random.randint(0, Bench.SCREEN_SIZE[0]-left)
        height = self.random.randint(0, Bench.SCREEN_SIZE[1]-top)
        color = self.random_color()
        self.rectangle(left, top, width, height, color)
    def random_line (self):
        start = (self.random.randint(0, Bench.SCREEN_SIZE[0]),
                 self.random.randint(0, Bench.SCREEN_SIZE[1]))
        end = (self.random.randint(0, Bench.SCREEN_SIZE[0]),
               self.random.randint(0, Bench.SCREEN_SIZE[1])) 
        color = self.random_color()
        self.line(start, end, color)
    def random_point (self):
        pos = (self.random.randint(0, Bench.SCREEN_SIZE[0]),
               self.random.randint(0, Bench.SCREEN_SIZE[1]))
        color = self.random_color()
        self.point(pos, color)
    def random_texture (self):
        pos = (self.random.randint(0, Bench.SCREEN_SIZE[0]),
               self.random.randint(0, Bench.SCREEN_SIZE[1]))
        tex_slot = self.random.randint(0, len(self.textures)-1)
        self.texture(pos, self.textures[tex_slot])
    def random_color (self):
        return self.color(self.random.randint(0x0, 0xFFFFFFFF))
    def flip (self):
        raise NotImplementedError
    def clear (self):
        raise NotImplementedError
    def color (self, value):
        raise NotImplementedError
    def fill (self, color):
        raise NotImplementedError
    def rectangle (self, left, top, width, height, color):
        raise NotImplementedError
    def line (self, start, end, color):
        raise NotImplementedError
    def point (self, pos, color):
        raise NotImplementedError
    def texture (self, pos, texture):
        raise NotImplementedError
    def load_texture (self, filename):
        raise NotImplementedError
        
